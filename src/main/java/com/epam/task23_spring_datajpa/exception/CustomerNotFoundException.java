package com.epam.task23_spring_datajpa.exception;

public class CustomerNotFoundException extends RuntimeException {

    public CustomerNotFoundException(){
    }

    public CustomerNotFoundException(String message) {
        super(message);
    }
}
