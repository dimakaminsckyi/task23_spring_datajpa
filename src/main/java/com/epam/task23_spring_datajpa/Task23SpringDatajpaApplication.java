package com.epam.task23_spring_datajpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task23SpringDatajpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(Task23SpringDatajpaApplication.class, args);
    }

}
