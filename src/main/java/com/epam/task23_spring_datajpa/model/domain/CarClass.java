package com.epam.task23_spring_datajpa.model.domain;

public enum CarClass {

    COMFORT,
    BUSINESS
}
