package com.epam.task23_spring_datajpa.model.domain;

public enum Payment {

    CARD,
    CASH
}
