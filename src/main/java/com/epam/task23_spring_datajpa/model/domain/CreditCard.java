package com.epam.task23_spring_datajpa.model.domain;

import javax.persistence.*;

@Entity
@Table(name = "credit_card")
public class CreditCard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "numbers", length = 16)
    private String numbers;
    @OneToOne(mappedBy = "card")
    private BusinessCustomer businessCustomer;
    @OneToOne(mappedBy = "card")
    private Driver driver;
    @OneToOne(mappedBy = "password")
    private Customer customer;

    public CreditCard() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumbers() {
        return numbers;
    }

    public void setNumbers(String numbers) {
        this.numbers = numbers;
    }
}
