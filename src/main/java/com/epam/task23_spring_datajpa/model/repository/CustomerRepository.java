package com.epam.task23_spring_datajpa.model.repository;

import com.epam.task23_spring_datajpa.model.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer > {

}
