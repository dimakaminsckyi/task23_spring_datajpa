package com.epam.task23_spring_datajpa.model.service;

import com.epam.task23_spring_datajpa.model.domain.Customer;

import java.util.List;

public interface CustomerService {

    List<Customer> getAllCustomers();

    void updateCustomer(Customer updateCustomer,Integer id);

    void deleteCustomer(Integer id);

    void saveCustomer(Customer customer);

    Customer getCustomerById(Integer id);
}
