package com.epam.task23_spring_datajpa.model.service.impl;

import com.epam.task23_spring_datajpa.exception.CustomerNotFoundException;
import com.epam.task23_spring_datajpa.model.domain.Customer;
import com.epam.task23_spring_datajpa.model.repository.CustomerRepository;
import com.epam.task23_spring_datajpa.model.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository repository;


    @Override
    public List<Customer> getAllCustomers() {
        Optional<List<Customer>> customers = Optional.ofNullable(repository.findAll());
        return customers.orElseThrow(CustomerNotFoundException::new);
    }

    @Override
    @Transactional
    public void updateCustomer(Customer updateCustomer, Integer id) {
        Customer customer = getCustomerById(id);
        customer.setCustomerName(updateCustomer.getCustomerName());
        customer.setPhoneNumber(updateCustomer.getPhoneNumber());
    }

    @Override
    @Transactional
    public void deleteCustomer(Integer id) {
        repository.delete(getCustomerById(id));
    }

    @Override
    @Transactional
    public void saveCustomer(Customer customer) {
        repository.save(customer);
    }

    @Override
    public Customer getCustomerById(Integer id) {
        Optional<Customer> customer = repository.findById(id);
        return customer.orElseThrow(CustomerNotFoundException::new);
    }

}
