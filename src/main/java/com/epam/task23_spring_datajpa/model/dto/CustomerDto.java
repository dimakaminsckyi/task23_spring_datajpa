package com.epam.task23_spring_datajpa.model.dto;

import com.epam.task23_spring_datajpa.controller.CustomerController;
import com.epam.task23_spring_datajpa.model.domain.Customer;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;


import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class CustomerDto extends ResourceSupport {
    Customer customer;

    public CustomerDto(Customer customer, Link selfLink) {
        this.customer = customer;
        add(selfLink);
        add(linkTo(methodOn(CustomerController.class).getCustomer(customer.getId())).withRel("customer"));
    }

    public Integer getCustomerId() {
        return customer.getId();
    }

    public String getCustomerName() {
        return customer.getCustomerName();
    }

    public String getCustomerPhoneNumber() {
        return customer.getPhoneNumber();
    }
}
