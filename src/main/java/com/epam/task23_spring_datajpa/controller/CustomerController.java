package com.epam.task23_spring_datajpa.controller;

import com.epam.task23_spring_datajpa.exception.CustomerNotFoundException;
import com.epam.task23_spring_datajpa.model.domain.Customer;
import com.epam.task23_spring_datajpa.model.dto.CustomerDto;
import com.epam.task23_spring_datajpa.model.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class CustomerController {

    @Autowired
    private CustomerService service;

    @PostMapping(value = "/api/customer")
    public ResponseEntity<CustomerDto> addCustomer(@RequestBody Customer newCustomer) {
        service.saveCustomer(newCustomer);
        Link link = linkTo(methodOn(CustomerController.class).getCustomer(newCustomer.getId())).withSelfRel();
        CustomerDto customerDto = new CustomerDto(newCustomer, link);
        return new ResponseEntity<>(customerDto, HttpStatus.CREATED);
    }

    @GetMapping(value = "/api/customer/{customerId}")
    public ResponseEntity<CustomerDto> getCustomer(@PathVariable Integer customerId) throws CustomerNotFoundException {
        Customer customer = service.getCustomerById(customerId);
        Link link = linkTo(methodOn(CustomerController.class).getCustomer(customerId)).withSelfRel();
        CustomerDto customerDto = new CustomerDto(customer, link);
        return new ResponseEntity<>(customerDto, HttpStatus.OK);
    }

    @PutMapping(value = "/api/customer/{customerId}")
    public ResponseEntity<CustomerDto> updateCustomer(@RequestBody Customer updateCustomer,
                                                  @PathVariable Integer customerId) throws CustomerNotFoundException {
        service.updateCustomer(updateCustomer, customerId);
        Customer customer = service.getCustomerById(customerId);
        Link link = linkTo(methodOn(CustomerController.class).getCustomer(customerId)).withSelfRel();
        CustomerDto customerDto = new CustomerDto(customer, link);
        return new ResponseEntity<>(customerDto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/api/customer/{customerId}")
    public  ResponseEntity deleteCustomer(@PathVariable Integer customerId) throws CustomerNotFoundException {
        service.deleteCustomer(customerId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping(value = "/api/customers")
    public ResponseEntity<List<CustomerDto>> getAllCustomers(){
        List<Customer> customers = service.getAllCustomers();
        Link link = linkTo(methodOn(CustomerController.class).getAllCustomers()).withSelfRel();
        List<CustomerDto> customerDtos = new ArrayList<>();
        for (Customer entity : customers) {
            Link selfLink = new Link(link.getHref() + "/" + entity.getId()).withSelfRel();
            CustomerDto dto = new CustomerDto(entity, selfLink);
            customerDtos.add(dto);
        }
        return new ResponseEntity<>(customerDtos, HttpStatus.OK);
    }
}
