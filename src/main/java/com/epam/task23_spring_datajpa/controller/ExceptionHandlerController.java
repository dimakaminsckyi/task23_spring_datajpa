package com.epam.task23_spring_datajpa.controller;

import com.epam.task23_spring_datajpa.exception.CustomerNotFoundException;
import com.epam.task23_spring_datajpa.model.dto.MessageDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CustomerNotFoundException.class)
    ResponseEntity<MessageDto> handleCustomerNotFound(){
        return new ResponseEntity<MessageDto>(new MessageDto("Customer not found"), HttpStatus.NOT_FOUND);
    }
}
